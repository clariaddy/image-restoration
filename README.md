# Basic Image Restoration application
## Abstract
This is a fork of https://github.com/shyama95/image-restoration with an additional non-GUI script to perform constrained least squares filtering on tiff stacks. The additional script to execute is named `restore.py` and requires python3.7 or later. The license is the same as in the original repository (GPLv3).

## Dependencies
- Git and git LFS to clone the repository including example tiff file
- Python3.7 or later with pip
- Python libraries : numpy, opencv, pillow (see `requirements.txt`).

## How to get started
1. Install git (should already be installed) and optionally [git-lfs](https://git-lfs.github.com/) (only required to obtain the test tiff file).
2. Install python3.7, python3.8, python3.9 or later (check if one is available on your system first). From now on `3.x` will denote your python version number, replace it with your version in the following commands.
3. Check that pip is installed for your target python version: `python3.x -m pip --version`. If pip is not found on your system, install it with:

    ```
    wget https://bootstrap.pypa.io/get-pip.py
    python3.x get-pip.py
    rm -f get-pip.py
    ```
4. Clone the repository with:
    ```
    git clone https://gitlab.com/clariaddy/image-restoration
    ```
5. Install python dependencies:
    ```
    cd image-restoration
    python3.x install --upgrade -r requirements.txt
    ```
6. Test the filter with provided tiff file (if you have git-lfs installed):
    ```
    git lfs install
    git lfs pull
    python3.x restore.py
    ```

## Usage
You can see usage with: `python3.x restore.py -h`

A sample input tiff should be available in the test folder. Please note that `git-lfs` is required to obtain the test tiff sample as described in the `How to get started` section.
