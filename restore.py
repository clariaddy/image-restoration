import os, sys, argparse, functools, logging
import numpy as np

import cv2
from PIL import Image, ImageSequence, TiffImagePlugin

def load_blur_kernel(args):
    logging.info(f" Loading blur kernel index {args.blur_kernel}.")
    kernel_filename = os.path.join(script_dir, 'kernels', f'{args.blur_kernel}.bmp')
    if not os.path.exists(kernel_filename):
        msg = f"Kernel file '{kernel_filename}' does not exist."
        raise RuntimeError(msg)
    kernel = np.array(cv2.imread(kernel_filename, 0))
    return kernel


def load_filter(args):
    logging.info(f" Loading filter {args.filter} with γ={args.gamma:2.2f}.")

    if (args.filter == 'constrained_ls_filter'):
        from imageRestorationFns import constrained_ls_filter
        blur_kernel = load_blur_kernel(args)
        img_filter = functools.partial(constrained_ls_filter, psf=blur_kernel, gamma=args.gamma)
    else:
        msg = f'Filter {args.filter} has not been implemented yet.'
        raise NotImplementedError(msg)
    return img_filter


def main(args):
    assert os.path.isfile(args.input_stack), args.input_stack
    assert not os.path.exists(args.output_stack), args.output_stack

    img_filter = load_filter(args)

    logging.info(f" Loading  input  stack '{args.input_stack}'.")
    input_img = Image.open(args.input_stack)

    logging.info(f" Creating output stack '{args.output_stack}'.")
    with TiffImagePlugin.AppendingTiffWriter(args.output_stack) as tf:
        logging.info(f" Iterating over {input_img.n_frames} input stack frames.")
        for (i, input_frame) in enumerate(ImageSequence.Iterator(input_img)):
            logging.info(f"    Filtering frame {i+1}/{input_img.n_frames}.")
            frame = np.array(input_frame)
            dtype = frame.dtype
            grayscale = (frame.ndim == 2)

            if grayscale:
                frame = np.dstack([frame]*3)

            frame = img_filter(frame)

            if grayscale:
                frame = frame[...,0]

            iinfo = np.iinfo(dtype)
            frame = np.minimum(iinfo.max, np.maximum(iinfo.min, np.rint(frame)))
            frame = frame.astype(dtype)

            input_frame.paste(Image.fromarray(frame))
            input_frame.save(tf)
            tf.newFrame()

            if (i+1 >= args.max_slices):
                break
    logging.info(f" All done.")

if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.realpath(__file__))

    test_dir = os.path.join(script_dir, 'test')
    default_input_stack = os.path.join(test_dir, 'test.tif')

    parser = argparse.ArgumentParser(description="Apply given filter to input image.")

    parser.add_argument("-i", "--input-stack", dest="input_stack", type=str,
        help="Specify input image as a tif file.", default=default_input_stack)

    parser.add_argument("-o", "--output-stack", dest="output_stack", type=str,
        help="Specify output image as a tif file, if not provided, a default name"
             "is deduced from input image name.", default=None)

    parser.add_argument("-x", "--overwrite-output", dest="force_overwrite",
            action="store_true", help="Overwrite output if it already exists.")

    parser.add_argument("-n", "--max-slices", dest="max_slices", type=float,
            default=np.inf, help="Maximum number of slices to filter (defaults to infinity).")

    parser.add_argument("-f", "--filter", dest="filter", type=str,
            default="constrained_ls_filter", help="Choose the filter to apply (defaults to constrained_ls_filter).")

    parser.add_argument("-b", "--blur-kernel", dest="blur_kernel", type=int,
            default=3, help="Specify blur kernel index value for constrained_ls_filter (defaults to 3).")

    parser.add_argument("-g", "--gamma", dest="gamma", type=float,
            default=12.0, help="Specify γ value for constrained_ls_filter (defaults to 12.0).")

    args = parser.parse_args()

    input_folder   = os.path.dirname(args.input_stack)
    input_filename = os.path.basename(args.input_stack)
    input_filename, input_extension = os.path.splitext(input_filename)

    if input_extension not in {'.tif', '.tiff'}:
        msg = f"Input stack has to be a *.tif or a *.tiff file, got '{args.input_stack}'."
        parser.error(msg)

    if not os.path.isfile(args.input_stack):
        msg = f"Input stack {args.input_stack} does not exist or is not a file."
        parser.error(msg)

    if (args.output_stack is None):
        args.output_stack = os.path.join(input_folder, f'{input_filename}.out{input_extension}')

    if os.path.exists(args.output_stack):
        if args.force_overwrite:
            if os.path.isfile(args.output_stack):
                os.remove(args.output_stack)
            else:
                msg = f"{args.output_stack} exist but is not a file, please remove manually."
                parser.error(msg)
        else:
            msg = f"Output stack '{args.output_stack}' already exists and may be overwritten by this script. "
            msg += "You have to specify -x or --overwrite-output to enforce output removal prior to filtering."
            parser.error(msg)
    else:
        output_dir = os.path.dirname(args.output_stack)
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)

    if (args.max_slices < 1.0):
        msg = f"Max slices has to be greater or equatl to one, got {args.max_slices}."
        parser.error(msg)

    if not (1 <= args.blur_kernel <= 5):
        msg = f"Blur kernel index have to lie between 1 and 5, got {args.blur_kernel}."
        parser.error(msg)

    if (args.gamma <= 0.0):
        msg = f"Gamma has to be strictly positive, got {args.gamma}."
        parser.error(msg)

    if sys.version_info < (3,7):
        raise RuntimeError("You need python3.7 or later to run this code.")
    elif sys.version_info >= (3,9):
        logging.basicConfig(encoding='utf-8', level=logging.INFO)
    else:
        logging.basicConfig(level=logging.INFO)

    main(args)

